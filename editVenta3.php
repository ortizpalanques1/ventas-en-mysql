<?php
$db = new mysqli('192.168.105.75', 'root', 'a', 'domingo');
$tabla = $db->query("select * from ventas order by vendedor ");
$html_table = '<table border="1" cellspacing="0" cellpadding="5"><tr><th>Factura</th><th>Vendedor</th><th>Cliente</th><th>Fecha</th><th>Importe</th></tr>';
?>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Clientes</title>
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <!-- Optional theme -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
    <style >
        #todosLosContactos{
            float: left;
            border: solid 3px #000000;
            background-color: #FFA07A;
            margin: 3px;
            width:600px;
            height:400px;
            overflow:auto;
            padding-left: 5px;
            transition: width 2s, height 2s, transform 2s;
            -webkit-transition: width 2s; /* Safari */
        }
        tr:nth-child(even) {background-color: #FFA07A;}
        tr:nth-child(odd) {background-color: #add8e6;}
</style>
</head>
<body style="background-color: #c2b280;">

<fieldset style="border: #061405" id="todosLosContactos"><legend >Lista de clientes</legend>
    <?php
    while ($fila = $tabla->fetch_assoc()){
    ?>
    <a href="editVenta.php?id=<?= $fila['id']?>">
        <?php $html_table .= '<tr><td>' .$fila['id']. '</td><td>' .$fila['vendedor']. '</td><td>' .$fila['cliente']. '</td><td>' .$fila['fecha']. '</td><td>' .$fila['importe']. '</td></tr>';}
        ?>
    </a>
        <?php
        $html_table .= '</table>';
        echo $html_table; 
        ?>     
</fieldset>
    </body>
    </html>

